package com.dao;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.jdbc.core.JdbcTemplate;

import com.data.Client;
import com.data.Deal;
import com.data.Event;

public class UseJDBC implements DataBaseDAO {

  @Autowired
  private JdbcTemplate jdbcTemplateObject;
  
  @Cacheable("clients")
  public Client getClient(Integer id){
  
  Client client = jdbcTemplateObject.queryForObject(
      "SELECT \"Client\".id, \"People\".name "
      + "FROM  public.\"Client\",public.\"People\" "
      + "WHERE \"Client\".id = \"People\".id AND \"Client\".id = ?;",new Object[] { id }, 
      new ClientMapper());
  System.out.println("Метод отработал и отдал по запросу клиента: " + client.name);
  return client;
  
  }

  @Override
  public void addDeal(Integer dealID, String date, Integer clientID, Integer employeeID,
      Integer parkingPlaceID, Integer carNumber) { //скорее всего здесь надо будет принимать просто экземпляр Deal
    DateTimeFormatter fmt = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    LocalDate localDate = LocalDate.parse(date, fmt);
    jdbcTemplateObject.update(
        "INSERT INTO public.\"Deals\" (id, date, client, employee, place, car) values (?, ?, ?, ?, ?, ?)",
        dealID, localDate, clientID, employeeID, parkingPlaceID, carNumber);
  }

  @Override
  public List<Deal> getDealsByClientID(Integer clientID) {
    
    List<Deal> deals = jdbcTemplateObject.query("SELECT "
        + "\"Deals\".id, " 
        + "\"Deals\".date, "
        + "\"Deals\".client, "
        + "\"Deals\".employee, "
        + "\"Deals\".place, "
        + "\"Deals\".car "
        + "FROM public.\"Deals\" "
        + "WHERE \"Deals\".client = ?", new Object[] { clientID }, new DealMapper());

    return deals;
  }

  @Override
  public void addPlaceEvent(Integer eventID, Integer placeID, String date, Event event) {
    DateTimeFormatter fmt = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    LocalDate localDate = LocalDate.parse(date, fmt);
    jdbcTemplateObject.update(
        "INSERT INTO public.\"PlaceEvents\" (id, place, date, event) values (?, ?, ?, ?)",
        eventID, placeID, localDate, event.getName());
  }
  
}
