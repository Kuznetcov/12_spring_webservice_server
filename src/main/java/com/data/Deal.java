package com.data;

import java.sql.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(name="Deal", propOrder={"dealID", "date", "clientID", "employeeID", "parkingPlaceID", "carNumber"})
@XmlAccessorType(XmlAccessType.FIELD)
public class Deal {

  @XmlElement(name="dealID")
  public Integer dealID;
  @XmlElement(name="date")
  public String date;
  @XmlElement(name="clientID")
  public Integer clientID;
  @XmlElement(name="employeeID")
  public Integer employeeID;
  @XmlElement(name="parkingPlaceID")
  public Integer parkingPlaceID;
  @XmlElement(name="carNumber")
  public Integer carNumber;
  
  
  public synchronized Integer getDealID() {
    return dealID;
  }
  public synchronized void setDealID(Integer dealID) {
    this.dealID = dealID;
  }
  public synchronized String getDate() {
    return date;
  }
  public synchronized void setDate(Date date) {
    System.out.println(date.toLocalDate());
    this.date = date.toLocalDate().toString();
  }
  public synchronized Integer getClientID() {
    return clientID;
  }
  public synchronized void setClientID(Integer clientID) {
    this.clientID = clientID;
  }
  public synchronized Integer getEmployeeID() {
    return employeeID;
  }
  public synchronized void setEmployeeID(Integer employeeID) {
    this.employeeID = employeeID;
  }
  public synchronized Integer getParkingPlaceID() {
    return parkingPlaceID;
  }
  public synchronized void setParkingPlaceID(Integer parkingPlaceID) {
    this.parkingPlaceID = parkingPlaceID;
  }
  public synchronized Integer getCarNumber() {
    return carNumber;
  }
  public synchronized void setCarNumber(Integer carNumber) {
    this.carNumber = carNumber;
  }
  
  
}
