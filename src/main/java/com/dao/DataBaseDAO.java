package com.dao;


import java.util.List;

import com.data.Client;
import com.data.Deal;
import com.data.Event;

public interface DataBaseDAO {

  public Client getClient(Integer id);
  
  public void addDeal(Integer dealID, String date, Integer clientID, Integer employeeID,
      Integer parkingPlaceID, Integer carNumber);
  
  public List<Deal> getDealsByClientID(Integer clientID);
  
  public void addPlaceEvent(Integer eventID, Integer placeID, String date, Event event);
  
  
}
