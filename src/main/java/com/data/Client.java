package com.data;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlAccessType;

@XmlType(name="Client", propOrder={"id", "name"})
@XmlAccessorType(XmlAccessType.FIELD)
public class Client {
  
  @XmlElement(name="id")
  public Integer id;
  @XmlElement(name="name")
  public String name;
  
  public synchronized Integer getId() {
    return id;
  }

  public synchronized void setId(Integer id) {
    this.id = id;
  }

  public synchronized String getName() {
    return name;
  }

  public synchronized void setName(String name) {
    this.name = name;
  }

  public Client() {}

  @Override
  public String toString() {
    return "Client [id=" + id + ", name=" + name + "]";
  }
  
}
